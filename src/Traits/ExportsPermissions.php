<?php

namespace mathewparet\LaravelPolicyAbilitiesExport\Traits;

use ReflectionMethod;
use Illuminate\Support\Facades\Gate;
use Illuminate\Database\Eloquent\Model;

trait ExportsPermissions
{
    protected static $global_abilities = [];

    protected static function getAbilities()
    {
        $model_class = get_called_class();

        $policy = Gate::getPolicyFor($model_class);

        $abilities = [];

        foreach (get_class_methods($policy) as $ability) {
            if (in_array($ability, ['before', 'denyWithStatus', 'denyAsNotFound'])) {
                continue;
            }

            $params = (new ReflectionMethod($policy, $ability))->getParameters();

            if (isset($params[1])) {
                $abilities[] = $ability;
            } else {
                self::$global_abilities[] = $ability;
            }
        }

        return $abilities;
    }

    public function getCanAttribute()
    {
        $permissions = [];

        foreach (self::getAbilities() as $ability) {
            $permissions[$ability] = auth()->user() ? auth()->user()->can($ability, $this) : false;
        }

        return $permissions;
    }

    public static function globalAbilities()
    {
        self::getAbilities();

        $permissions = [];

        foreach (self::$global_abilities as $ability) {
            $permissions[$ability] = auth()->user() ? auth()->user()->can($ability, get_called_class()) : false;
        }

        return $permissions;
    }

    public static function bootExportsPermissions()
    {
        static::retrieved(function (Model $model) {
            $model->append(['can']);
        });
    }
}