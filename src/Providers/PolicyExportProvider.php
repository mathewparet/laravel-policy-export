<?php
namespace mathewparet\LaravelPolicyAbilitiesExport\Providers;

use Illuminate\Support\ServiceProvider;
use mathewparet\LaravelPolicyAbilitiesExport\Console\MakeResourceCollectionCommand;

class PolicyExportProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('command.make:resource-collection', MakeResourceCollectionCommand::class);

        $this->commands([
            'command.make:resource-collection'
        ]);
    }
}