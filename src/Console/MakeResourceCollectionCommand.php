<?php

namespace mathewparet\LaravelPolicyAbilitiesExport\Console;

use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class MakeResourceCollectionCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:resource-collection {--model=} {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a resource collection with permissions';

    protected $type = 'ResourceCollection';

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);

        $modelClass = $this->parseModel($this->option('model'));

        $search = [
            'DummyResourceCollection',
            '{{ model_class }}',
            '{{ namespacedModel }}',
        ];

        $replacements = [
            $this->argument('name'),
            class_basename($modelClass),
            $modelClass,
        ];

        return str_replace($search, $replacements, $stub);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/ResourceCollectionWithPermissions.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Http\Resources';
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    protected function parseModel($model)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $model)) {
            throw new InvalidArgumentException('Model name contains invalid characters.');
        }

        return $this->qualifyModel($model);
    }
}
