<?php
namespace mathewparet\LaravelPolicyAbilitiesExport\Resources;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ResourceCollectionWithPermissions extends ResourceCollection
{
    protected $model;

    public function toArray(Request $request): array
    {
        return [
            'data' => $this->collection,
            'can' => $this->model::globalAbilities()
        ];
    }

}