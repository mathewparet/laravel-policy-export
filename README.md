# mathewparet/LaravelPolicyAbilitiesExport

This package exports all laravel policy defined abilities as a JSON object for VueJS to process.

# Installation

Require the module:

```shell
composer require mathewparet/laravel-policy-abilities-export
```

Add the `ExportsPermissions` trait to your model

```php
use mathewparet\LaravelPolicyAbilitiesExport\Traits\ExportsPermissions;
// ...
class Blog extends Model
{
    use ExportsPermissions;
}
```

# Usage

The model will now have a `can` attribute that lists all abilities for that record.

For global abilities that are tied to the model but not the record itself (E.g., Blog Create), call `Model::globalAbilities()`

To create a resource collection that would export global abilities along with it, run:

```sh
php artisan make:resource-collection [--model=MODEL] name
```